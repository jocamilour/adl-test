# ADL-test

Pueba tecnica ADL Frontend, creada con create-react-app y redux. 
El Json para la prueba fue corregido y se sube a mock.io para simular el consumo a un API

### Pre-requisitos 📋

1. NodeJS **10.15.3** o superior
2. NPM **6.9.0** o superior


### Instalación 🔧

1. Clonar el repositorio

```shell
git clone <https://gitlab.com/jocamilour/adl-test.git>
```
2. Instalar las dependencias

```shell
npm install
```
3. Probar en entorno local

```shell
npm start
```

### Ejecutando las pruebas ⚙️

Para ejecutar las pruebas de unidad con Jest, se debe escribir el siguiente comando:
```shell
NODE_ENV=<env> npm run test
```

## Construido con 🛠️

* [create-react-app](https://create-react-app.dev) - Framework de ReactJS usado
* [NPM](https://www.npmjs.com/) - Manejador de dependencias


## Autor ✒️

* **Jose Camilo Urrego** - *Desarrollo Sitio* - [josurr](https://gitlab.com/jocamilour)


---