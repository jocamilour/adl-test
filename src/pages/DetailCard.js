import React, { Component } from 'react'
import axios from 'axios'
import { getProducts } from '../store/action'
import { connect } from 'react-redux'

const PRODUCT_SERVICE = process.env.REACT_APP_PRODUCT_SERVICE

class DetailCard extends Component {

  componentDidMount () {
    this.getProduct()
  }

  getProduct = () => {
    axios.get(PRODUCT_SERVICE)
      .then(
        response => {
          this.props.getProducts(response.data)
        }
      ).catch(error => {
      console.log('error =>', error)
    })
  }

  render () {
    const id = this.props.match.params.id

    if (typeof this.props.product.product !== 'undefined') {
      console.log(this.props.product.product[id])
    }

    return (
      <div>
        el detalle del producto {id}
        <li>
        </li>
      </div>

    )
  }
}

function mapStateToProps (state) {
  return {
    product: state.product.product
  }
}

export default connect(mapStateToProps, { getProducts })(DetailCard)