import React, { Component } from 'react'
import axios from 'axios'
import { getProducts } from '../store/action'
import { connect } from 'react-redux'

import ListCards from '../components/ListCards'
import FinanceView from '../components/FinanceView'

const PRODUCT_SERVICE = process.env.REACT_APP_PRODUCT_SERVICE

class Home extends Component {

  componentDidMount () {
    this.getProduct()
  }

  getProduct = () => {
    axios.get(PRODUCT_SERVICE)
      .then(
        response => {
          this.props.getProducts(response.data)
        }
      ).catch(error => {
      console.log('error =>', error)
    })
  }

  render () {
    return (
      <div id={'content'}>
        <div className="col-11 align-center">
          <FinanceView/>
          <ListCards product={this.props.product}/>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    product: state.product.product
  }
}

export default connect(mapStateToProps, { getProducts })(Home)