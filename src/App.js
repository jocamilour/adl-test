import React from 'react'
import Routes from './routes'

function App () {
  return (
    <div className="container d-flex">
      <div id="menu"> Menu</div>
      <Routes />
    </div>
  )
}

export default App
