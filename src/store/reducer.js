const initialState = { product: [] }

// action es el valor devuelto por el action
//action.payload será el valor que quiero añadir, borrar, etc
export function product (state = initialState, action) {
  if (action.type === 'GET_PRODUCT') {
    return {
      ...state, //Lo que devuelve un reducer es lo que se quedará en el state, por tanto, debe devolver todo lo que había antes (además de la información que cambia)
      product: action.payload
    }
  }

  return state
}

// export const selectActiveWord = state => state.palabraReducer.palabra