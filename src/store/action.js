// Las acciones siempre tienen la estructura { type, payload }
export const getProducts = (product) => {
  return {
    type: 'GET_PRODUCT',
    payload: product
  }
};