import React from 'react'
import { Link } from 'react-router-dom'

export default function CreditCard (props) {

  const { card, index } = props
  return (
    <div className="card bounceIn" key={index}>
      <div className="d-flex margin-b">
        <div className="col-6">
          <img className="col-6"
               src={(parseInt(card.id.charAt(0)) === 4) ? 'visa.png' : 'master.png'}
               alt={''}/>
        </div>
        <Link className="btn col-6" to={`/detail/` + index}>Ver detalles »</Link>
      </div>
      <div className="margin-b">
        <h3>Tarjeta <br/>de Crédito</h3>
        <h6>Nro {'xxxx xxxx xxxx ' + card.id.substring(12, 16)}</h6>
      </div>

      <div className="margin-b">
        <h6>Saldo disponible</h6>
        <h4>${card.productAccountBalances.cupo_disponible_compras_pesos.amount}</h4>
      </div>
      <div className="d-flex">
        <div className="d-flex col-6">
          <div><h6>Fecha de corte</h6></div>
          <div><h5>{card.dueDate}</h5></div>
        </div>
        <div className="d-flex col-6">
          <div><h6>Saldo total</h6></div>
          <div><h5>{card.productAccountBalances.saldo_actual.amount}</h5></div>
        </div>
      </div>
    </div>
  )
}