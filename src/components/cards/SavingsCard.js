import React from 'react'
import { Link } from 'react-router-dom'

export default function SavingsCard (props) {
    const { card, index } = props

    return (
      <div className="card bounceIn" key={index}>
        <div className="d-flex margin-b">
          <div className="col-6"><img className="col-6" src="logo.png" alt={''}/></div>
          <Link className="btn col-6" to={`/detail/` + index} card={card}>Ver detalles »</Link>
        </div>
        <div className="margin-b">
          <h3>Cuenta <br/> de Ahorros</h3>
          <h6>Nro {card.id}</h6>
        </div>

        <div className="margin-b">
          <h6>Saldo disponible</h6>
          <h4>${card.productAccountBalances.saldo_disponible.amount}</h4>
        </div>
        <div className="d-flex">
          <div className="d-flex col-6">
            <div><h6>En bolsillos</h6></div>
            <div><h5>${card.productAccountBalances.saldo_ayer.amount}</h5></div>
          </div>
          <div className="d-flex col-6">
            <div><h6>Saldo total</h6></div>
            <div><h5>${card.productAccountBalances.saldo_actual.amount}</h5></div>
          </div>
        </div>
      </div>
    )
}