import React from 'react'
import '../App.css'
import SavingsCard from './cards/SavingsCard'
import CurrentAccountCard from './cards/CurrentAccountCard'
import CdtCard from './cards/CdtCard'
import CreditCard from './cards/CreditCard'
import CommonCreditCard from './cards/CommonCreditCard'

export default function ListCards (props) {

  const { product } = props
  return (
    <div id="card bounceIn" className={'align-center'}>
      {
        typeof product.product !== 'undefined' ?
          Object.keys(product.product).map((index) => {
              switch (product.product[index].typeAccount) {
                case 'DEPOSIT_ACCOUNT':
                  return <SavingsCard card={product.product[index]} index={index} key={index}/>

                case 'CURRENT_ACCOUNT':
                  return <CurrentAccountCard card={product.product[index]} index={index} key={index}/>

                case 'CERTIFIED_DEPOSIT_TERM':
                  return <CdtCard card={product.product[index]} index={index} key={index}/>

                case 'CREDIT_CARD':
                  return <CreditCard card={product.product[index]} index={index} key={index}/>

                case 'CREDIT':
                  return <CommonCreditCard card={product.product[index]} index={index} key={index}/>

                default:
                  return null
              }

            }
          )
          : null
      }
    </div>
  )
}