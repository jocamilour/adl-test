import React from 'react'

export default function FinanceView (props) {
  return (
    <div className="align-center col-10 label">

      <div className="col-8"><h5>¿Cómo se ven tus finanzas?</h5></div>
      <div className="d-flex col-4">
        <img alt={''}/>
        <h6>En productos <br/>de ahorro tienes</h6>
        <h3>$2.351.000,00</h3>
        <img alt={''}/>
      </div>

    </div>
  )
}