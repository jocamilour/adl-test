import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import DetailCard from './pages/DetailCard';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/detail/:id" component={DetailCard} />
      <Route path="*" component={() => <div>404</div> } />
    </Switch>
  </BrowserRouter>
);

export default Routes;